CC = gcc
FLAGS = -Wall -Werror -g
FILES = $(wildcard *.c)
OBJS = $(FILES:.c=.o)
TARGET = libgeneric_data_structures.so

all: lib

clean: 
	-rm -f *.o $(TARGET) &> /dev/null ; cd tests ; $(MAKE) clean ; cd ../examples ; $(MAKE) clean

lib: $(OBJS)
	$(CC) -shared $(OBJS) -o $(TARGET)

lib32: $(OBJS)
	$(CC) -m32 -shared $(OBJS) -o $(TARGET)

%.o: %.c 
	$(CC) -c $(FLAGS) -fPIC $< -o $@ 

install: lib
	mv $(TARGET) /usr/lib/

tests: $(OBJS)
	cd tests && $(MAKE) && ./tests

.PHONY: all tests clean examples

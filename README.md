### Generic C Data Structures (libgeneric_data_structures)

## 1. Generic C Array

It can hold any type of elements, given a size. It will automatically double it's size, when it reaches the current allocated value, as well as halfing the memory used when reaching a quarter of usage. To print specific types, a printing function must be provided, as in the example file.


To install the shared library on your system: (sudo) make install

To run the tests: make tests (written in generic_array_tests.c)

To use the library, check the example file and Makefile. You should include the header and use -lgeneric_array in your compiling process.

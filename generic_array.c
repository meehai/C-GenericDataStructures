#include "generic_array.h"

void ga_create_array(generic_array_t *ga_array, size_t element_size){
	ga_array->array = malloc(GA_ARRAY_DEFAULT_MAX_SIZE * element_size);
	assert(ga_array->array && "Allocation error");

	ga_array->current_size = 0;
	ga_array->max_size = GA_ARRAY_DEFAULT_MAX_SIZE;
	ga_array->element_size = element_size;
}

void ga_add_element(generic_array_t *ga_array, void *element){

	if(ga_array->current_size == ga_array->max_size){
		ga_array->max_size *= 2;
		ga_array->array = realloc(ga_array->array, ga_array->max_size * ga_array->element_size);
		assert(ga_array->array && "Allocation error");
	}

	memcpy(ga_array->array + ga_array->current_size * ga_array->element_size, element, ga_array->element_size);
	ga_array->current_size++;
}

void ga_clear_array(generic_array_t *ga_array) {
	free(ga_array->array);
	ga_array->array = NULL;
	ga_array->current_size = 0;
}

void ga_remove_element(generic_array_t *ga_array, int index) {
	int diff;
	if(index >= ga_array->current_size || index < 0)
		return;

	diff = ga_array->current_size - index;
	memmove(ga_array->array + index * ga_array->element_size, ga_array->array + (index + 1) * ga_array->element_size,
		   (diff - 1) * ga_array->element_size );
	ga_array->current_size--;
}

generic_array_t *ga_deepcopy(generic_array_t original) {
	generic_array_t *new_array = malloc(sizeof(generic_array_t));
	if(!new_array)
		return NULL;

	ga_create_array(new_array, original.element_size);
	new_array->current_size = original.current_size;
	memcpy(new_array->array, original.array, original.current_size * original.element_size);

	return new_array;
}

void ga_print_array(generic_array_t ga_array, void (*func)(void *arg)){
	int i;
	void* elem;

	if(ga_array.current_size == 0){
		printf("[]\n");
		return;
	}

	printf("[");
	elem = malloc(ga_array.element_size);
	assert(elem);

	memcpy(elem, ga_array.array, ga_array.element_size);
	func(elem);

	for(i=1; i<ga_array.current_size; i++) {
		memcpy(elem, ga_array.array + i * ga_array.element_size, ga_array.element_size);
		printf(", ");
		func(elem);
	}

	printf("]\n");
	free(elem);
}

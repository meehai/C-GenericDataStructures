/* Generic Array - Pirvu Mihai Cristian */
#ifndef GENERIC_ARRAY_H_
#define GENERIC_ARRAY_H_	1

#include "generic.h"

#define GA_ARRAY_DEFAULT_MAX_SIZE 10

struct generic_array {
	size_t current_size;
	size_t max_size;
	size_t element_size;
	void* array;
};

typedef struct generic_array generic_array_t;

void ga_create_array(generic_array_t *, size_t);
void ga_add_element(generic_array_t *, void *);
#define ga_get_element(ga_array, type, index) ( *(type*)(((ga_array).array) + sizeof(type) * (index)) )
#define ga_remove_first(ga_array) ( ga_remove_element((ga_array), 0) )
#define ga_remove_last(ga_array) ( ga_remove_element((ga_array), (ga_array)->current_size-1) )
void ga_remove_element(generic_array_t *, int);
void ga_clear_array(generic_array_t *);
void ga_print_array(generic_array_t, void (*func)(void *));
generic_array_t *ga_deepcopy(generic_array_t);

#endif

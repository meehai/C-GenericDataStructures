#include "generic_list.h"

void gl_create_list(generic_list_head_t *list, size_t element_size) {
	list->element_size = element_size;
	list->current_size = 0;
	list->first = NULL;
	list->last = NULL;
}

void gl_add_element(generic_list_head_t *list, void *element) {
	struct node *node = NULL;
	assert(element);
	
	node = malloc(sizeof(struct node));
	assert(node && "Couldn't malloc");
	node->next = NULL;
	node->value = malloc(list->element_size);
	assert(node->value && "Couldn't malloc");
	memcpy(node->value, element, list->element_size);
	list->current_size++;

	if(list->first == NULL) {
		list->first = node;
		list->last = node;
		node->prev = NULL;
	}
	else {
		list->last->next = node;
		node->prev = list->last;
		list->last = node;
	}
}

void gl_remove_element(generic_list_head_t *list, int index) {
	struct node *ptr_prev, *ptr;

	if (index < 0 || index >= list->current_size)
		return;
	ptr = NULL;

	/* remove first element, we must also update the list head */
	if(index == 0) {
		ptr = list->first;
		list->first = list->first->next;
	}

	/* remove the last element */
	if(index == list->current_size - 1) {
		ptr = list->last;
		list->last = list->last->prev;
	}

	/* If the index wasn't first or last (or both), means it's somewhere inside the list */
	if(ptr == NULL) {
		ptr_prev = gl_get_node(list, index-1);
		ptr = ptr_prev->next;
		ptr_prev->next = ptr->next;
		/* ptr_prev -> ptr -> NULL */
		if(ptr->next) {
			ptr->next->prev = ptr_prev;
		}
	}

	list->current_size--;
	free(ptr->value);
	free(ptr);
}

void gl_clear_list(generic_list_head_t *list) {
	int i, old_size;
	
	old_size = list->current_size;
	for(i=0; i<old_size; i++) {
		gl_remove_element(list, 0);
	}
	list->first = list->last = NULL;
}

generic_list_head_t *gl_deepcopy(generic_list_head_t list) {
	generic_list_head_t *new_list;
	struct node* ptr;
	
	new_list = malloc(sizeof(generic_list_head_t));
	assert(new_list && "malloc");
	gl_create_list(new_list, list.element_size);

	for(ptr=list.first; ptr != NULL; ptr = ptr->next) {
		gl_add_element(new_list, ptr->value);
	}

	return new_list;
}

void gl_print_list(generic_list_head_t list, void (*func)(void *)) {
	struct node *ptr;
	ptr = list.first;

	if(list.current_size == 0){
		printf("[]\n");
		return;
	}

	printf("[");
	func(ptr->value);
	ptr = ptr->next;
	while(ptr != list.last->next) {
		printf(", ");
		func(ptr->value);
		ptr = ptr->next;
	}
	printf("]\n");
}

struct node *gl_get_node(generic_list_head_t *list, int index) {
	int i;
	struct node* ptr;

	if(index >= list->current_size)
		return NULL;

	ptr = list->first;
	for(i=0; i<index; i++)
		ptr=ptr->next;

	return ptr;
}

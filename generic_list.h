/* Generic List - Pirvu Mihai Cristian */
#ifndef GENERIC_LIST_H_
#define GENERIC_LIST_H_	1

#include "generic.h"

struct generic_list_head {
	struct node *first;
	struct node *last;
	size_t element_size;
	size_t current_size;
};

struct node {
	struct node *next;
	struct node *prev;
	void *value;
};

typedef struct generic_list_head generic_list_head_t;

void gl_create_list(generic_list_head_t *, size_t);
void gl_add_element(generic_list_head_t *, void *);
#define gl_get_element(gl_list_head, type, index) ( *(type*)((gl_get_node((gl_list_head), (index)))->value) )
struct node *gl_get_node(generic_list_head_t *, int);
#define gl_remove_first(gl_list_head) ( gl_remove_element((gl_list_head), 0) )
#define gl_remove_last(gl_list_head) ( gl_remove_element((gl_list_head), (gl_list_head)->current_size-1) )
void gl_remove_element(generic_list_head_t *, int);
void gl_clear_list(generic_list_head_t *);
void gl_print_list(generic_list_head_t, void (*func)(void *));
generic_list_head_t *gl_deepcopy(generic_list_head_t);

#endif

#include "tests.h"

int main(int argc, char **argv) {
	runArrayTests();
	runListTests();

	return 0;
}

#include <stdlib.h>
#include <time.h>
#include "../generic_array.h"

static int testInitialization() {
	printf("Test initialization");
	generic_array_t array;
	int size = rand() % 20, ok;

	ga_create_array(&array, size);
	ok = array.array != NULL && array.current_size == 0 && array.max_size == GA_ARRAY_DEFAULT_MAX_SIZE && 
		array.element_size == size;
	ga_clear_array(&array);

	return ok;
}

static int testAddElement() {
	printf("Test add element");
	generic_array_t array;
	char c;
	int ok;

	ga_create_array(&array, sizeof(char));
	for(c='a'; c<='z'; c++)
		ga_add_element(&array, &c);

	ok = array.current_size == 'z' - 'a' + 1 && ga_get_element(array, char, 0) == 'a' && 
		ga_get_element(array, char, 10) == 'k' && ga_get_element(array, char, array.current_size - 1) == 'z';
	ga_clear_array(&array);

	return ok;
}

static int testRemoveElement() {
	printf("Test remove element");
	generic_array_t array;
	int i, ok;

	ga_create_array(&array, sizeof(int));
	for(i=0; i<10; i++)
		ga_add_element(&array, &i);
	ga_remove_first(&array);
	ga_remove_last(&array);
	ga_remove_element(&array, 3);

	ok = array.current_size = 7 && ga_get_element(array, int, 0) == 1 &&
		ga_get_element(array, int, array.current_size - 1) == 8 && ga_get_element(array, int, 3) == 5;
	ga_clear_array(&array);

	return ok;
}

static int testDeepCopy() {
	printf("Test deep copy");
	generic_array_t array, *copy;
	int i;

	ga_create_array(&array, sizeof(int));
	for(i=0; i<10; i++)
		ga_add_element(&array, &i);
	copy = ga_deepcopy(array);
	ga_remove_first(&array);

	for(i=0; i<9; i++)
		if(ga_get_element(array, int, i) != ga_get_element(*copy, int, i+1))
			return 0;

	ga_clear_array(&array);
	ga_clear_array(copy);
	free(copy);

	return 1;
}

int runArrayTests(){
	int (*tests[])() = {testInitialization, testAddElement, testRemoveElement, testDeepCopy};
	int i, N;

	srand(time(NULL));
	N = sizeof(tests) / sizeof(tests[0]);

	printf("\nRunning Array tests...\n");
	for (i=0; i<N; i++) {
		printf("%20s\t%s\n", "", tests[i]() == 1 ? "OK" : "NOK");
	}

	return 0;
}

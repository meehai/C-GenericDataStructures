#include <stdio.h>
#include <time.h>
#include "../generic_list.h"

struct Test {
	char message[100];
	int (*test)();
};

static int testInitialization() {
	generic_list_head_t list;
	int size = rand() % 20, ok;

	gl_create_list(&list, size);
	ok = list.first == NULL && list.last == NULL && list.current_size == 0  && list.element_size == size;
	gl_clear_list(&list);
	return ok;
}

static int testAddElement() {
	generic_list_head_t list;
	int size = rand() % 20, ok = 1, i;

	gl_create_list(&list, sizeof(int));
	for(i=0; i<size; i++)
		gl_add_element(&list, &i);
	for(i=0; i<size; i++)
		ok = ok && gl_get_element(&list, int, i) == i;

	gl_clear_list(&list);
	ok = ok && list.first == NULL && list.last == NULL && list.current_size == 0  && list.element_size == sizeof(int);
	return ok;
}

static int testRemoveElement() {
	generic_list_head_t list;
	int size = rand() % 20, ok = 1, i;

	gl_create_list(&list, sizeof(int));
	for(i=0; i<size; i++)
		gl_add_element(&list, &i);
	for(i=0; i<size; i+=2) {
		gl_remove_element(&list, i);
		i--;
	}
	size = list.current_size;
	for(i=0; i<size; i++) {
		ok = ok && gl_get_element(&list, int, i) % 2 == 1;
	}

	gl_clear_list(&list);
	return ok;
}

static int testDeepCopy() {
	generic_list_head_t list, *copy = NULL;
	int size = rand() % 20, ok = 1, i;

	gl_create_list(&list, sizeof(int));
	for(i=0; i<size; i++)
		gl_add_element(&list, &i);

	copy = gl_deepcopy(list);
	gl_clear_list(&list);
	for(i=0; i<size; i++)
		ok = ok && gl_get_element(copy, int, i) == i;

	return ok;
}

static int testRemoveOnlyFirst() {
	generic_list_head_t list;
	int x = rand() % 20, ok = 1;

	gl_create_list(&list, sizeof(int));
	gl_add_element(&list, &x);
	gl_remove_element(&list, 0);
	ok = ok && list.current_size == 0;

	gl_add_element(&list, &x);
	ok = ok && gl_get_element(&list, int, 0) == x;

	gl_clear_list(&list);

	return ok;
}

int runListTests() {
	struct Test tests[] = { 
		{"Test Initialization", testInitialization},
		{"Test Add Element", testAddElement},
		{"Test Remove Element", testRemoveElement},
		{"Test Deep Copy", testDeepCopy},
		{"Test Remove Only First", testRemoveOnlyFirst}
	};
	int i, N;

	srand(time(NULL));
	N = sizeof(tests) / sizeof(tests[0]);

	printf("\nRunning List tests...\n");
	for (i=0; i<N; i++) {
		struct Test test = tests[i];
		test.message[100] = '\0';
		printf("%s%20s\t%s\n", test.message, "", test.test() == 1 ? "OK" : "NOK");
	}

	return 0;
}
